import { createMuiTheme } from "@material-ui/core";

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#681FD7"
        },
        secondary: {
            main: "#8dd71f"
        }
    }
});

export default theme;