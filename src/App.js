import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import Counter from './components/counter/Counter';
import { Container, makeStyles, CssBaseline, Paper, ThemeProvider } from '@material-ui/core';
import store from './store/store';
import theme from './constants/theme';

const useStyles = makeStyles(theme => ({
  container: {
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
}));

function App() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Container className={classes.container}>
          <Counter />
        </Container>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
