import React, { useState, useRef } from 'react';
import { makeStyles, Card, CardContent, TextField } from '@material-ui/core';
import { connect } from 'react-redux';
import { setCountAction } from '../../store/actions';

const useStyles = makeStyles(theme => ({
    counter: {
        width: 300,
        height: 150,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: theme.palette.primary.dark
    },
    counterText: {
        color: theme.palette.secondary.light,
        fontSize: theme.typography.h3.fontSize,
        fontWeight: "bold",
        textAlign: "center"
    }
}));


const mapStateToProps = (state) => ({
    count: state.count,
    interval: state.interval
});

const mapDispatchToProps = (dispatch) => ({
    setCount: count => dispatch(setCountAction(count))
});

function CounterView(props) {
    const classes = useStyles();
    const [newCount, setNewCount] = useState("");

    //select the correct unit
    let counterUnit = '';
    if (props.interval === 1000)
        counterUnit = 's';
    else if (props.interval === 100)
        counterUnit = '00ms';

    let countDisplayVal = String(props.count) + counterUnit;
    //add comma divider
    if (counterUnit === '00ms') {
        if (props.count === 0)
            countDisplayVal = '0ms';
        else if (countDisplayVal.length > 5)
            countDisplayVal = countDisplayVal.slice(0, countDisplayVal.length - 5) + ',' + countDisplayVal.slice(countDisplayVal.length - 5);
    }
    else if (counterUnit === 's') {
        if (countDisplayVal.length > 4)
            countDisplayVal = countDisplayVal.slice(0, countDisplayVal.length - 4) + ',' + countDisplayVal.slice(countDisplayVal.length - 4);
    }
    else if (counterUnit === '') {
        if (countDisplayVal.length > 3)
            countDisplayVal = countDisplayVal.slice(0, countDisplayVal.length - 3) + ',' + countDisplayVal.slice(countDisplayVal.length - 3);
    }

    function handleNewCount(e) {
        const stringVal = e.target.value;
        const val = parseInt(stringVal);
        if (!isNaN(val) || !stringVal || stringVal === '-')
            setNewCount(stringVal);
    }

    function handleCountUpdate(e) {
        e.preventDefault();
        const val = parseInt(newCount.substr(0, newCount.length));
        if (!isNaN(val)) {
            if (props.interval === 100)
                props.setCount(Math.floor(val / 100)); // divide input by 100 when interval is 100ms to match it
            else
                props.setCount(val);
        }
        setNewCount('');

    };

    return (
        <Card className={classes.counter}>
            <CardContent>
                <form onSubmit={handleCountUpdate}>
                    <TextField className={classes.counterText} size="medium" variant="standard" fullWidth type="text" placeholder={countDisplayVal} value={newCount} onChange={handleNewCount} onBlur={handleCountUpdate}
                        inputProps={{ style: { textAlign: "center", fontSize: 50 } }} />
                </form>
            </CardContent>
        </Card>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(CounterView);