import React, { useState } from 'react';
import { makeStyles, Button, Slider, Typography, TextField, Grid, FormControlLabel, Switch } from '@material-ui/core';
import { setIntervalAction, setStepAction, setCountAction, setFreshAction, setPausedAction, setDirectionAction } from '../../store/actions';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
    inputBoxContainer: {
        position: "relative",
        width: "100%",
        marginTop: theme.spacing(1)
    },
    inputBox: {
        marginTop: theme.spacing(1),
        border: "solid",
        borderRadius: 5,
        borderWidth: 1,
        padding: theme.spacing(1),
        paddingLeft: theme.spacing(2.5),
        paddingRight: theme.spacing(2.5),
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        borderColor: theme.palette.text.hint,
        '&:hover': {
            borderColor: theme.palette.secondary.main,
        }
    },
    inputTitle: {
        position: "absolute",
        top: 0,
        left: theme.spacing(1),
        lineHeight: "normal",
        paddingRight: theme.spacing(0.5),
        paddingLeft: theme.spacing(0.5),
        textTransform: "capitalize",
        color: theme.palette.text.hint,
        backgroundColor: theme.palette.background.paper
    }
}));

const mapStateToProps = (state) => ({
    interval: state.interval,
    step: state.step,
    isFresh: state.isFresh,
    isPaused: state.isPaused,
    direction: state.direction
});

const mapDispatchToProps = (dispatch) => ({
    setCount: count => dispatch(setCountAction(count)),
    setInterval: interval => dispatch(setIntervalAction(interval)),
    setStep: step => dispatch(setStepAction(step)),
    setFresh: isFresh => dispatch(setFreshAction(isFresh)),
    setPaused: isPaused => dispatch(setPausedAction(isPaused)),
    setDirection: direction => dispatch(setDirectionAction(direction))
});

const marks = [

    {
        label: "0.1s", // minimum safe interval supported by browsers
        value: 100
    },
    {
        label: "1s",
        value: 1000
    },
    {
        label: "2s",
        value: 2000
    },
    {
        label: "3s",
        value: 3000
    }
];



function ControlPane(props) {
    const classes = useStyles();

    const startPauseText = props.isFresh ? "Start" : props.isPaused ? "Resume" : "Pause";

    function handleSlider(e, value) {
        props.setInterval(value);
    };

    function handleDirSwitch() {
        props.setDirection(!props.direction);
    }

    const pause = () => props.setPaused(true);
    const resume = () => props.setPaused(false);

    function handleStartPause() {
        props.setFresh(false);
        if (props.isPaused) resume();
        else pause();
    };

    function handleReset() {
        props.setFresh(true);
        props.setCount(0);
        props.setInterval(1000);
        props.setStep(1);
        pause();
    };

    return (
        <>
            <div className={classes.inputBoxContainer}>
                <Typography className={classes.inputTitle} variant="overline" align="center">Interval</Typography>
                <div className={classes.inputBox}>
                    <Slider
                        onChange={handleSlider}
                        valueLabelDisplay="auto"
                        color="secondary"
                        value={props.interval}
                        step={50}
                        min={100}
                        max={3000}
                        marks={marks}
                    />
                </div>
            </div>
            <div className={classes.inputBoxContainer}>
                <Typography className={classes.inputTitle} variant="overline" align="center">Direction</Typography>
                <div className={classes.inputBox}>
                    <Typography>-</Typography>
                    <Switch color="secondary" checked={props.direction} onChange={handleDirSwitch} />
                    <Typography>+</Typography>
                </div>
            </div>
            <Grid style={{ marginTop: 8 }} container spacing={1}>
                <Grid item xs={3}>
                    <Button variant="outlined" color="secondary" fullWidth onClick={handleReset}>Reset</Button>
                </Grid>
                <Grid item xs={1}></Grid>
                <Grid item xs={8}>
                    <Button variant="contained" color="secondary" fullWidth
                        onClick={handleStartPause}>
                        {startPauseText}
                    </Button>
                </Grid>
            </Grid>
        </>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(ControlPane);