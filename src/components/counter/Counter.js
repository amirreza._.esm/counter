import React from 'react';
import { makeStyles, Paper } from '@material-ui/core';
import CounterView from './CounterView';
import ControlPane from './ControlPane';
import { useInterval } from '../../hooks/custom';
import { setCountAction } from '../../store/actions';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    }
}));

const mapStateToProps = (state) => ({
    count: state.count,
    interval: state.interval,
    step: state.step,
    isPaused: state.isPaused,
    direction: state.direction
});

const mapDispatchToProps = (dispatch) => ({
    setCount: (count) => dispatch(setCountAction(count))
});


function Counter(props) {
    const classes = useStyles();

    function tick() { props.setCount(props.count + (props.direction ? props.step : props.step * -1)); };

    useInterval(tick, props.isPaused ? null : props.interval);

    return (
        <Paper className={classes.root}>
            <CounterView />
            <ControlPane />
        </Paper>
    );
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter);