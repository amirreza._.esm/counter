import { useRef, useEffect } from "react";

export function useInterval(handler, ms) {
    const handlerRef = useRef();

    useEffect(() => {
        handlerRef.current = handler;
    });

    useEffect(() => {
        if (ms !== null) {
            const id = setInterval(() => { handlerRef.current() }, ms);
            return () => {
                clearInterval(id);
            };
        }
    }, [ms]);
};