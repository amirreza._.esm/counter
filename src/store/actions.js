export const SET_COUNT = "set_count";
export const SET_INTERVAL = "set_interval";
export const SET_STEP = "set_step";
export const SET_FRESH = "set_fresh";
export const SET_PAUSED = "set_paused";
export const SET_DIRECTION = "set_direction";

export function setCountAction(count) {
    return ({
        type: SET_COUNT,
        value: count
    });
}

export function setIntervalAction(interval) {
    return ({
        type: SET_INTERVAL,
        value: interval
    });
}

export function setStepAction(step) {
    return ({
        type: SET_STEP,
        value: step
    });
}

export function setFreshAction(isFresh) {
    return ({
        type: SET_FRESH,
        value: isFresh
    });
}

export function setPausedAction(isPaused) {
    return ({
        type: SET_PAUSED,
        value: isPaused
    });
}

export function setDirectionAction(direction) {
    return ({
        type: SET_DIRECTION,
        value: direction
    });
}