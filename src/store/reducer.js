import { SET_COUNT, SET_INTERVAL, SET_STEP, SET_FRESH, SET_PAUSED, SET_DIRECTION } from "./actions";

const initialState = {
    count: 0,           //counter value
    interval: 1000,     //delay in ms
    step: 1,            //counter step
    isFresh: true,      //used for control labels
    isPaused: true,
    direction: true     //true=positive false=negative
}

export function reducer(state = initialState, action) {
    let newState;
    switch (action.type) {
        case SET_COUNT:
            newState = Object.assign({}, state, { count: action.value });
            break;
        case SET_INTERVAL:
            newState = Object.assign({}, state, { interval: action.value });
            break;
        case SET_STEP:
            newState = Object.assign({}, state, { step: action.value });
            break;
        case SET_FRESH:
            newState = Object.assign({}, state, { isFresh: action.value });
            break;
        case SET_PAUSED:
            newState = Object.assign({}, state, { isPaused: action.value });
            break;
        case SET_DIRECTION:
            newState = Object.assign({}, state, { direction: action.value });
            break;

        default:
            newState = state;
            break;
    }
    return newState;
}